editAreaLoader.load_syntax["oxpath"] = {
	'DISPLAY_NAME' : 'OXPATH'
	,'COMMENT_SINGLE' : {1 : '____'}
	,'COMMENT_MULTI' : {'/**' : '**/'}
	,'QUOTEMARKS' : ['"', "'"]
	,'KEYWORD_CASE_SENSITIVE' : false
	,'KEYWORDS' : {
		'attributes' : ['doc','click']
		,'values' : ['input','descendant']
		,'specials' : ['important']
	}
	,'OPERATORS' :[
		':', ';', '!', '.', '#','/'
	]
	,'DELIMITERS' :[
		'{', '}','(',')','{','}'
	]
	,'STYLES' : {
		'COMMENTS': 'color: #AAAAAA;'
		,'QUOTESMARKS': 'color: #6381F8;'
		,'KEYWORDS' : {
			'attributes' : 'color: #48BDDF;'
			,'values' : 'color: #2B60FF;'
			,'specials' : 'color: #FF0000;'
			}
		,'OPERATORS' : 'color: #FF00FF;'
		,'DELIMITERS' : 'color: #60CA00;'
				
	}
};
